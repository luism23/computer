import Content from "@components/content"
import Banner from "@components/banner"
import Contacts from "@components/contacts2"
import Suscription from "@components/suscription"
import Space from "@components/space"

import Info from "@data/contacts"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner} className="banner-gradient-1"/>
                <Space px={21}/>
                <Contacts {...Info.contacts}/>
                <Space px={123}/>
                <Suscription {...Info.suscription}/>
                <Space px={144}/>
            </Content>
        </>
    )
}
export default Index