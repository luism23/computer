import Content from "@components/content"
import Banner from "@components/banner"
import ServicesInfo from "@components/servicesInfo"
import Contacts from "@components/contacts"
import Suscription from "@components/suscription"
import Space from "@components/space"

import Info from "@data/services"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner} className="banner-variation-2 banner-variation-4"/>
                <Space px={102}/>
                <ServicesInfo {...Info.servicesInfo}/>
                <Contacts {...Info.contacts}/>
                <Space px={46}/>
                <Suscription {...Info.suscription}/>
                <Space px={119}/>
            </Content>
        </>
    )
}
export default Index