import Content from "@components/content"
import Banner from "@components/banner"
import ContentInfo from "@components/contentInfo"
import Services from "@components/services"
import Contacts from "@components/contacts"
import Space from "@components/space"

import Info from "@data/aboust-us"

const Index = () => {
    return (
        <>
            <Content>
                    <Banner {...Info.banner} className="banner-variation-2"/>
                <div className="bg-white-lite">
                    <ContentInfo {...Info.contentInfo} className="contentInfo-variation-3 contentInfo-border"/>
                    <ContentInfo {...Info.contentInfo2} className="contentInfo-variation-3 contentInfo-float-left"/>

                    <Space px={92}/>
                </div>
                <Services {...Info.services}/>
                <Space px={301}/>
                <Contacts {...Info.contacts}/>
                <Space px={123}/>
            </Content>
        </>
    )
}
export default Index