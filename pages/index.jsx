import Content from "@components/content"
import Banner from "@components/banner"
import ContentInfo from "@components/contentInfo"
import SliderImg from "@components/sliderImg"
import Contacts from "@components/contacts"
import Suscription from "@components/suscription"
import Space from "@components/space"

import Info from "@data/home"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner}/>
                <ContentInfo {...Info.contentInfo}/>
                <SliderImg {...Info.slider1}/>
                <Space px={150}/>
                <Contacts {...Info.contacts}/>
                <Space px={72}/>
                <SliderImg {...Info.slider2}/>
                <Space px={150}/>
                <Suscription {...Info.suscription}/>
                <Space px={119}/>
            </Content>
        </>
    )
}
export default Index