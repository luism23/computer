import Content from "@components/content"
import SliderImg from "@components/sliderImg"
import Cart from "@components/cart"
import Contacts from "@components/contacts"
import Suscription from "@components/suscription"
import Space from "@components/space"

import Info from "@data/products"

const Index = () => {
    return (
        <>
            <Content>
                <SliderImg {...Info.slider1} class="slider1-variation-5"  />
                <Cart {...Info.cart}/>
                <Space px={150}/>
                <Contacts {...Info.contacts}/>
                <Space px={72}/>
                <Suscription {...Info.suscription}/>
                <Space px={119}/>
            </Content>
        </>
    )
}
export default Index