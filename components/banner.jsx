import SourceImg from "@components/sourceimg"

const Index = ({ bg, title, className = "" ,idScroll = ""}) => {
    return (
        <>
            <div id="banner" className={`banner  ${className}`}>
                <picture className="banner-fondo">
                    <SourceImg
                        img={bg}
                        className="banner-fondo-img"
                    />
                </picture>
                <div className="container">
                    <div className="banner-content  bg-black-dark color-black-dark" >
                        <h1 className="banner-content-title color-white font-montserrat font-w-500 font-96">
                            {title}
                        </h1>
                        <a className=" btnHover banner-buttom color-white" href={`#${idScroll}`}>
                            <img src="/svg/buttom.svg" alt="buttom" />
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index