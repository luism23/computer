import Link from "next/link"

import Source from "@components/sourceimg"

const LinkFooter = ({link,text}) => {
    return (
        <Link href={link}>
            <a className="linkHover footer-content-link color-white font-24 font-montserrat font-w-500">
                {text}
            </a>
        </Link>
    )
}

const ColFooter = ({title="",links=[]}) => {
    return (
        <div className="footer-content-col">
            <h3 className="footer-content-title color-white font-36 font-montserrat font-w-500">
                {title}
            </h3>
            {
                links.map((e,i)=>{
                    return (
                        <LinkFooter
                            key={i}
                            {...e}
                        />
                    )
                })
            }
        </div>
    )
}

const Index = ({ logo, cols = [] }) => {
    return (
        <>

            <footer className=" footer bg-gray-dark">
                <div className="footer-content container">
                    <picture className="footer-content-logo">
                        <Source
                            img={logo}
                            name="logo"
                        />
                    </picture>
                    {
                        cols.map((e,i)=>{
                            return (
                                <ColFooter
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </footer>
        </>
    )
}
export default Index