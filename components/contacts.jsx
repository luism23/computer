import Source from "@components/sourceimg"

const Index = ({ title , text  }) => {
    return (
        <>
            <div className="contacts">
                <picture className="contacts-img">
                    <Source
                    img="lenovo2.png"
                    name="lenovo2"
                    />
                </picture>
                <div className="container contacts-container">
                    <div className="contacts-void"></div>
                    <div className="contacts-content">
                        <h4 className="contacts-title color-white font-96 font-montserrat font-w-500 ">
                            {title}
                        </h4>
                        <p className="contacts-info font-24 font-montserrat color-white">
                            {text}
                        </p>
                        <input className="contacts-input border-color-white bg-black font-36 color-white font-w-500 font-montserrat" type="text" placeholder="Name" />
                        <input className="contacts-input border-color-white bg-black font-36 color-white font-w-500 font-montserrat" type="email" placeholder="Email" />
                        <input className="btnHover contacts-input submit border-color-white bg-white-lite-2 font-36 color-black font-w-500 font-montserrat" type="submit" value="Send" />
                    </div>
                </div>

            </div>
        </>
    )
}
export default Index