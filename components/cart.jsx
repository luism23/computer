import Source from "@components/sourceimg"


const Carts = ({ img, title, link, text , subPrice, price}) => {
    return (

        <div>
            <div className="cart">
                <picture>
                    <Source
                        img={img}
                        name={title}
                    />
                </picture>

                <div className="">
                    <h4 className="font-48 font-w-500 font-montserrat">
                        {title}
                    </h4>
                    <p className=" font-24 font-montserrat ">
                        {text}
                    </p>
                    <p>
                        {subPrice}
                    </p>
                    <p>
                        {price}
                    </p>
                </div>


            </div>
        </div>




    )
}



const Index = ({ img, title, link, text }) => {
    return (
        <>
            <div className="cart">
               
            </div>
        </>
    )
}
export default Index