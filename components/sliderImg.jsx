import Link from "next/link"
import { useEffect } from "react"

import Source from "@components/sourceimg"

const ItemSlider = ({img,title,link}) => {
    return (
        <div className="carousel-cell">
            <div className="ItemSlider">
                <div className="container ItemSlider-content">
                    <picture className="ItemSlider-img">
                        <Source
                            img={img}
                            name={title}
                        />
                    </picture>
                    <h4 className="ItemSlider-title font-96 font-montserrat font-w-500 color-white">{title}</h4>
                    <Link href={link}>
                        <a className="btnHover ItemSlider-btn color-white border-color-white bg-black font-36 font-montserrat font-w-500">
                            Buy
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    )
}


const Index = ({items,id=""}) => {
    useEffect(() => {
        const Flickity = require("flickity")
        var flky = new Flickity(`#${id}`,{});
    }, [])
    return (
        <>
            <div className="container">
                <div id={id} className="carousel slider">
                    {
                        items.map((e,i)=>{
                            return (
                                <ItemSlider
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index