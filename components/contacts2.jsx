const Index = ({ text, id }) => {
    return (
        <>
            <div className="contacts2">
                <div className="idScroll" id={id}></div>
                <div className="container contacts2-container">
                    <div className="contacts2-content">
                        <p className="contacts2-info font-24 font-montserrat color-white">
                            {text}
                        </p>
                        <div className="contacts2-content-input">
                            <input className="contacts2-input border-color-white bg-black font-36 color-white font-w-500 font-montserrat" type="text" placeholder="Name" />
                            <input className="contacts2-input border-color-white bg-black font-36 color-white font-w-500 font-montserrat" type="email" placeholder="Email" />
                        </div>
                        <textarea className="contacts2-input input-textarea border-color-white bg-black font-36 color-white font-w-500 font-montserrat" name="mjs" placeholder="Msj"></textarea>
                        <div className="content-submit">
                            <input className="btnHover contacts2-input submit border-color-white bg-black font-36 color-black bg-white-lite-2 font-w-500 font-montserrat" type="submit" value="Send" />
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}
export default Index