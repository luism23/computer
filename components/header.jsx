import Link from "next/link"

import Source from "@components/sourceimg"
import { useState } from "react"

const NavLink = ({ title, link }) => {
    return (
        <Link href={link}>
            <a className="nav-link font-montserrat font-24 color-white linkHover">
                {title}
            </a>
        </Link>
    )
}

const Index = ({ logo = "", links = [] }) => {
    return (
        <>
            <header className=" header bg-black-dark">
                <div className="header-content container">
                    <Link href="/">
                        <a className="header-content-logo">
                            <picture >
                                <Source
                                    img={logo}
                                    name="logo"
                                    className="header-logo"
                                />
                            </picture>
                        </a>
                    </Link>

                    <button className="header-btn" onClick={() => { document.querySelector("html").classList.toggle("activeMenu") }}></button>
                    <nav className={`font-24 header-nav color-white`}>
                        {
                            links.map((e, i) => {
                                return (
                                    <NavLink
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </nav>
                </div>
            </header>
        </>
    )
}
export default Index