import Source from "@components/sourceimg"

const ServicesInfo = ({ title, text, img }) => {
    return (
        <div className="servicesInfo-content">
            <div className="servicesInfo-content-imgTitle">
                <h2 className="servicesInfo-content-imgTitle-title color-white font-64 font-w-500 font-montserrat ">{title}</h2 >
                <picture className="servicesInfo-content-imgTitle-img">
                    <Source
                        img={img}
                        name={title}

                    />
                </picture>

            </div>
            <p className="servicesInfo-content-info color-white font-24 font-montserrat ">
                {text}</p>
        </div>


    )

}

const Index = ({ infoS = [], id = "" }) => {
    return (
        <>
            <div className="servicesInfo container">
                {
                    infoS.map((e, i) => {
                        return (
                            <ServicesInfo
                                key={i}
                                {...e}
                            />
                        )
                    })
                }

            </div>
        </>
    )
}
export default Index