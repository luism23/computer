import Source from "@components/sourceimg"

const Index = ({ title , }) => {
    return (
        <>
            <div className="suscription container">
                <picture className="suscription-img">
                    <Source
                        img="lenovo4.png"
                        name="lenovo4"
                    />
                </picture>
                <h4 className="suscription-title font-96 font-w-500 color-white font-montserrat">
                    {title}
                </h4>
                <div className="suscription-content-inputs">
                    <div className="void"/>
                    <input className="suscription-input color-white font-montserrat font-36 font-w-500 bg-black border-color-white" type="Email" placeholder="Email" />
                    <input className="suscription-input color-black font-montserrat font-36 font-w-500 bg-white-lite-2 submit  border-color-white btnHover" type="submit" placeholder="Send" />
                    <div className="void"/>

                </div>
            </div>
        </>
    )
}
export default Index