import Link from "next/link"

import Source from "@components/sourceimg"


const LinkServices = ({ link, text }) => {
    return (
        <Link href={link}>
            <a className="services-content-2-link-info color-black font-36 font-montserrat"> {text}</a>
        </Link>
    )

}


const Index = ({ title = "", services = [] }) => {
    return (
        <>
            <div className="services ">
                <div className="services-content-1">
                    <picture className="services-img">
                        <Source
                            img="services.png"
                            name="services"
                        />
                    </picture>
                    <div className="container">
                    <h2 className="services-content-1-title color-black font-96 font-w-500 font-montserrat ">
                        {title}
                    </h2>
                    </div>
                </div>
                <div className="bg-white-lite">

                    <div className="services-content-2 container">
                        {
                            services.map((e, i) => {
                                return (
                                    <LinkServices
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index