import SourceImg from "@components/sourceimg"

const Index = ({ title,text , id , className=""}) => {
    return (
        <>
            <div className={`contentInfo container  ${className}`}>
                <div className="idScroll"  id={id}></div>
                <div className="contentInfo-content  bg-white-lite">
                    <h1 className="contentInfo-title font-96 font-montserrat color-black font-w-500">
                        {title}
                    </h1>
                    <p className="contentInfo-info font-24 font-montserrat ">
                       {text}
                    </p>
                </div>
            </div>
        </>
    )
}
export default Index