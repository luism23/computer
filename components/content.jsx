import Header from "@components/header"
import Footer from "@components/footer"

import headerInfo from "@data/header"
import footerInfo from "@data/footer"

const Index = ({ children, header = true, footer = true }) => {
    return (
        <>
            {header && <Header {...headerInfo}/>}
            {children}
            {footer &&  <Footer {...footerInfo}/>}
        </>
    )
}
export default Index