export default {
    logo: "logo2.png",
    cols: [
        {
            title: "Services",
            links: [
                {
                    link: "/",
                    text: "Products"
                },
                {
                    link: "/",
                    text: "Maintenance"
                },
                {
                    link: "/",
                    text: "Soport"
                }
            ]
        },
        {
            title: "Contact",
            links: [
                {
                    link: "/",
                    text: "email@gmail.com"
                },
                {
                    link: "/",
                    text: "+57 111 111 1111"
                },
                {
                    link: "/",
                    text: "@instagram"
                }
            ]
        }
    ]
}