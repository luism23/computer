export default {
    banner: {
        title: (
            <>
                Hello!........
                <br />
                <span style={{ marginLeft: "var(--pl-span,6.5rem)" }}>What your need?</span>
            </>
        ),
        bg: "fondo.png",
        idScroll:"contentInfo"
    },
    contentInfo: {
        id:"contentInfo",
        title: "We have the your need!",
        text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ipsam beatae, debitis praesentium ipsum eos. Nostrum consequuntur expedita quo minima tempora a cum. Optio cumque ullam quia nisi unde vero! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat."
    },
    slider1: {
        id: "slider1",
        items: [
            {
                img: "lenovo.png",
                title: "Lenovo",
                link: "#"
            },
            {
                img: "lenovo.png",
                title: "Acer",
                link: "#"
            },
            {
                img: "lenovo.png",
                title: "Dell",
                link: "#"
            },
            {
                img: "lenovo.png",
                title: "Hp",
                link: "#"
            },
        ]
    },
    contacts: {
        title: "Contacts",
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quia sunt pariatur fugit minus libero earum labore, eveniet accusantium ut nulla! Eos unde esse maxime aut porro sequi? Natus, voluptatum."
    },
    slider2: {
        id: "slider2",
        items: [
            {
                img: "lenovo3.png",
                title: "Lenovo",
                link: "#"
            },
            {
                img: "lenovo3.png",
                title: "Acer",
                link: "#"
            },
            {
                img: "lenovo3.png",
                title: "Dell",
                link: "#"
            },
            {
                img: "lenovo3.png",
                title: "Hp",
                link: "#"
            },
        ]
    },
    suscription: {
        title: "Suscription"
    }
}