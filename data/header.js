export default {
    logo: "logo.png",
    links: [
        {
            title: "About us",
            link: "/aboust-us"
        },
        {
            title: "Contact",
            link: "/contacts"
        },
        {
            title: "Services",
            link: "/services"
        },
        {
            title: "Products",
            link: "/products"
        }    
    ]
}