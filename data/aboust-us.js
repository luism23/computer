export default {
    banner: {
        title: (
            <>
                About us
            </>
        ),
        bg: "abouts-us-portada.png",
        idScroll:"contentInfo"
    },
    contentInfo: {
        id:"contentInfo",
        title: "We have the your need!",
        text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ipsam beatae, debitis praesentium ipsum eos. Nostrum consequuntur expedita quo minima tempora a cum. Optio cumque ullam quia nisi unde vero! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat."
    },
    contentInfo2: {
        title: "We have the your need!",
        text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ipsam beatae, debitis praesentium ipsum eos. Nostrum consequuntur expedita quo minima tempora a cum. Optio cumque ullam quia nisi unde vero! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium totam labore odio, hic iure ducimus ipsa voluptatibus, unde blanditiis illum porro pariatur consequuntur nemo reiciendis veniam minus rerum? Ratione, placeat."
    },
    services:{
        title:"What are our services?",
        services:[
            {
                text:"Services one",
                link:"/"
            },
            {
                text:"Services two",
                link:"/"
            },
            {
                text:"Services three",
                link:"/"
            },
            {
                text:"Services foor",
                link:"/"
            },
            {
                text:"Services five",
                link:"/"
            },
            {
                text:"Services six",
                link:"/"
            },
            {
                text:"Services seven",
                link:"/"
            },
            {
                text:"Services eight",
                link:"/"
            },
            {
                text:"Services nine",
                link:"/"
            },
            {
                text:"Services eleven",
                link:"/"
            },
            {
                text:"Services twelve",
                link:"/"
            },
            {
                text:"Services one",
                link:"/"
            },
        ]
    },
    contacts: {
        title: "Contacts",
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quia sunt pariatur fugit minus libero earum labore, eveniet accusantium ut nulla! Eos unde esse maxime aut porro sequi? Natus, voluptatum."
    },
    
    
}