export default {
    slider1: {
        id: "slider1",
        items: [
            {
                img: "products-img.png",
                title: "Desktop",
                link: "#"
            },
            {
                img: "products-img.png",
                title: "Desktop",
                link: "#"
            },
            {
                img: "products-img.png",
                title: "Desktop",
                link: "#"
            },
            {
                img: "products-img.png",
                title: "Desktop",
                link: "#"
            },
        ]
    },
    cart:{
        img:"cart-img.png", 
        title:"Lenovo", 
        link:"#", 
        text: "", 
        subPrice:"600",
         price:"499",
    },

    contacts: {
        title: "Contacts",
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quia sunt pariatur fugit minus libero earum labore, eveniet accusantium ut nulla! Eos unde esse maxime aut porro sequi? Natus, voluptatum."
    },
   
    suscription: {
        title: "Suscription"
    }
}