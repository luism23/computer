export default {
    banner: {
        title: (
            <>
                Services
            </>
        ),
        bg: "services-portada.png",
        idScroll: "contentInfo"
    },
    servicesInfo: {
        id: "infoSer",
        infoS: [
            {
                img: "services-img.png",
                title: "Services one",
                text: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic,Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet "
            },
            {
                img: "services-img.png",
                title: "Services two",
                text: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic,Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet "
            },
            {
                img: "services-img.png",
                title: "Services three",
                text: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic,Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet"
            },
            {
                img: "services-img.png",
                title: "Services four",
                text: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic,Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet consectetur adipisic Lorem ipsum, dolor sit amet consectetur adipisicadipisicing elit. Delectus qui nihil vel minima tempora unde, quidem aspernatur non libero voluptate? Aliquid officia, mollitia harum unde possimus cupiditate placeat quod odio. Lorem ipsum, dolor sit amet"
            }
        ]

    },

    contacts: {
        title: "Contacts",
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quia sunt pariatur fugit minus libero earum labore, eveniet accusantium ut nulla! Eos unde esse maxime aut porro sequi? Natus, voluptatum."
    },

    suscription: {
        title: "Suscription"
    }
}